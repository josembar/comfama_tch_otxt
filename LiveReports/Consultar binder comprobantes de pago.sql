select DT.Name from csadmin.DTree as DT
inner join csadmin.LLAttrData LLCategoria on
LLCategoria.ID = DT.DataID
where LLCategoria.DefID = 457918
and DT.SubType = 31066
and LLCategoria.AttrID = 2
and LLCategoria.ValInt = cast(%1 as int) --- atributo a ingresar

/** En el livereport se crea el parámetro como string, pero el campo en la tabla es numérico.
Por esta razón se debe hacer un cast. Al intentar capturar el campo como numérico desde el
perfil de escaneo con LLInteger este falló **/