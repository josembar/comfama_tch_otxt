import Ixos.Scan.Indexing;
Field.Choices.Clear();
Field.Choices.Add(new IndexingPair(null, "<Ninguno>"));
Field.Choices.Add("En trámite");
Field.Choices.Add("Pagado");
Field.Choices.Add("Objetado");
Field.Choices.Add("Cerrado");