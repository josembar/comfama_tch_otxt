//facturas sd >> procesamiento >> extensión de secuencia


//Script Secuencia extension de procesamiento Conteo de imagenes escaneadas 
var escaneado:int = 0
var escaneado = Context["ESCANEADO"];
escaneado = escaneado + 1;

Context["ESCANEADO"] = escaneado;

//Campo caja >> fase selección

//Field.ReadOnly = true;
//Script Indexando -> Secuencia Seleccion para ejecutar el reporte  LR_ES_Insertar_Estadistica
import System.Windows.Forms

var paginasEscaneadas:int = 0;
paginasEscaneadas = Context["ESCANEADO"];

if (paginasEscaneadas > 0){
	var resultado:int = insertarEstadistica("Escaneo",paginasEscaneadas);
	if(resultado > 0 )
	{
		Context["ESCANEADO"] = 0;
	}
}


//Script Funcion Global para ejecutar el reporte  LR_ES_Insertar_Estadistica
//ADD Referencias
//C:\Program Files (x86)\OpenText\Scan\bin\LLApi.dll
//C:\Program Files (x86)\OpenText\Scan\bin\ScanExtensionLivelink.dll

import System.Windows.Forms
import OpenText.Imaging.LLAPI;
import Ixos.Scan.Extension.Livelink
import System.Globalization;

function insertarEstadistica(in_actividad:String, in_paginas:String) 
{
	var paginasEscaneadas:int = 0;
	var fecha: DateTime = DateTime.Now; 
	var ci:CultureInfo = CultureInfo.InvariantCulture;
	
	//Toma los datos de la session con el content
		var session2: ILapiSession = SessionManager.Session;

	//se inicia sesion de LLAPI
		var session:LLSocketSession = new LLSocketSession(session2.Server, 2099, session2.Username, session2.Password, "");
 
	//Se usa la sesion para crear la instancia a LapiDocuments
		var doccuments:LapiDocuments = new LapiDocuments(session);

	//Variables para ejecutar reporte
		var listatributos:LLList = new LLList();
		var atributo:LLAssoc = new LLAssoc();
		var resultado:LLRecArray = new LLRecArray();
		var volid:long = -16943; //ID del Volumen
		var nodeid:long  = 197757;//ID del Reporte;
	
	//Atributos Constantes por perfil
		var Actividad:String = in_actividad//Escaneado o Archivado;
		var Perfil_Escaneo:String = "Perfil_Facturas SD";
		var Tipo_Formato:String = "PDF";
	
	//Calculo de valores de atributos
		var Fecha_Cargue:String = fecha.ToString("yyyy-MM-dd hh:mm:ss.fff tt",ci);
		var Estacion:String = System.Environment.MachineName;
		var Usuario:String = System.Environment.UserName;
		var Cantidad_Paginas:String = in_paginas;
		var Cantidad_Imagenes:String = "";
		var Tamano:String = "";
  
	//Agragamos los valores de cada atributo
		//Actividad
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel1"));
			atributo.Add("Value", new LLString(Actividad));
			atributo.Add("Prompt", new LLString("Actividad"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Perfil_Escaneo
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel2"));
			atributo.Add("Value", new LLString(Perfil_Escaneo));
			atributo.Add("Prompt", new LLString("Perfil_Escaneo"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Fecha_Cargue
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel3"));
			atributo.Add("Value", new LLString(Fecha_Cargue));
			atributo.Add("Prompt", new LLString("Fecha_Cargue"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Estacion
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel4"));
			atributo.Add("Value", new LLString(Estacion));
			atributo.Add("Prompt", new LLString("Estacion"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Usuario
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel5"));
			atributo.Add("Value", new LLString(Usuario));
			atributo.Add("Prompt", new LLString("Usuario"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
	
		//Tipo_Formato
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel6"));
			atributo.Add("Value", new LLString(Tipo_Formato));
			atributo.Add("Prompt", new LLString("Tipo_Formato"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Cantidad_Paginas
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel7"));
			atributo.Add("Value", new LLString(Cantidad_Paginas));
			atributo.Add("Prompt", new LLString("Cantidad_Paginas"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
			
		//Cantidad_Paginas
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel8"));
			atributo.Add("Value", new LLString(Cantidad_Imagenes));
			atributo.Add("Prompt", new LLString("Cantidad_Imagenes"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
			
		//Tamano
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel9"));
			atributo.Add("Value", new LLString(Tamano));
			atributo.Add("Prompt", new LLString("Tamano"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();

    	try 
	{
		//Ejecución del reporte LR_ES_Insertar_Estadistica
		resultado = doccuments.RunReport(volid, nodeid, listatributos);
        session.SessionFree;
		return resultado.RowCount;
	}
	catch(e)
	{
			session.SessionFree;
			return 0;
	}


}


//Caja >> secuencia de prearchivado

//Script Secuencia Prearchivado para ejecutar el reporte  LR_ES_Insertar_Estadistica
import System.Collections;
import System.Windows.Forms;
import Ixos.Scan.DocumentModel;

var selectedDoc:IDocumentSelection;
var doc:IDocument;
var total_paginas:int = 0;
var i:int;

selectedDoc = Application.ArchivingDocuments; 

for(i = 0; i<selectedDoc.Count; i++)
{
    doc= selectedDoc[i];
    total_paginas = total_paginas + doc.PageCount;
}

if (total_paginas > 0){
	insertarEstadistica("Archivado",total_paginas);
}


//Script Funcion Global para ejecutar el reporte  LR_ES_Insertar_Estadistica
//ADD Referencias
//C:\Program Files (x86)\OpenText\Scan\bin\LLApi.dll
//C:\Program Files (x86)\OpenText\Scan\bin\ScanExtensionLivelink.dll

import System.Windows.Forms
import OpenText.Imaging.LLAPI;
import Ixos.Scan.Extension.Livelink
import System.Globalization;

function insertarEstadistica(in_actividad:String, in_paginas:String) 
{
	var paginasEscaneadas:int = 0;
	var fecha: DateTime = DateTime.Now; 
	var ci:CultureInfo = CultureInfo.InvariantCulture;
	
	//Toma los datos de la session con el content
		var session2: ILapiSession = SessionManager.Session;

	//se inicia sesion de LLAPI
		var session:LLSocketSession = new LLSocketSession(session2.Server, 2099, session2.Username, session2.Password, "");
 
	//Se usa la sesion para crear la instancia a LapiDocuments
		var doccuments:LapiDocuments = new LapiDocuments(session);

	//Variables para ejecutar reporte
		var listatributos:LLList = new LLList();
		var atributo:LLAssoc = new LLAssoc();
		var resultado:LLRecArray = new LLRecArray();
		var volid:long = -16943; //ID del Volumen
		var nodeid:long  = 197757;//ID del Reporte;
	
	//Atributos Constantes por perfil
		var Actividad:String = in_actividad//Escaneado o Archivado;
		var Perfil_Escaneo:String = "Perfil_Facturas SD";
		var Tipo_Formato:String = "PDF";
	
	//Calculo de valores de atributos
		var Fecha_Cargue:String = fecha.ToString("yyyy-MM-dd hh:mm:ss.fff tt",ci);
		var Estacion:String = System.Environment.MachineName;
		var Usuario:String = System.Environment.UserName;
		var Cantidad_Paginas:String = in_paginas;
		var Cantidad_Imagenes:String = "";
		var Tamano:String = "";
  
	//Agragamos los valores de cada atributo
		//Actividad
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel1"));
			atributo.Add("Value", new LLString(Actividad));
			atributo.Add("Prompt", new LLString("Actividad"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Perfil_Escaneo
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel2"));
			atributo.Add("Value", new LLString(Perfil_Escaneo));
			atributo.Add("Prompt", new LLString("Perfil_Escaneo"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Fecha_Cargue
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel3"));
			atributo.Add("Value", new LLString(Fecha_Cargue));
			atributo.Add("Prompt", new LLString("Fecha_Cargue"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Estacion
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel4"));
			atributo.Add("Value", new LLString(Estacion));
			atributo.Add("Prompt", new LLString("Estacion"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Usuario
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel5"));
			atributo.Add("Value", new LLString(Usuario));
			atributo.Add("Prompt", new LLString("Usuario"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
	
		//Tipo_Formato
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel6"));
			atributo.Add("Value", new LLString(Tipo_Formato));
			atributo.Add("Prompt", new LLString("Tipo_Formato"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
	
		//Cantidad_Paginas
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel7"));
			atributo.Add("Value", new LLString(Cantidad_Paginas));
			atributo.Add("Prompt", new LLString("Cantidad_Paginas"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
			
		//Cantidad_Paginas
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel8"));
			atributo.Add("Value", new LLString(Cantidad_Imagenes));
			atributo.Add("Prompt", new LLString("Cantidad_Imagenes"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();
			
		//Tamano
			atributo.Add("InputType", new LLString("String"));
			atributo.Add("Label", new LLString("inputLabel9"));
			atributo.Add("Value", new LLString(Tamano));
			atributo.Add("Prompt", new LLString("Tamano"));

			//Agregamos el atributo a la lista
			listatributos.Add(atributo);
			atributo = new LLAssoc();

    	try 
	{
		//Ejecución del reporte LR_ES_Insertar_Estadistica
		resultado = doccuments.RunReport(volid, nodeid, listatributos);
        session.SessionFree;
		return resultado.RowCount;
	}
	catch(e)
	{
			session.SessionFree;
			return 0;
	}


}