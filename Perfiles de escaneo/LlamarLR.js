import OpenText.Imaging.LLAPI;
import System.Windows.Forms;
import Ixos.Scan.Extension.Livelink

var session2: ILapiSession = SessionManager.Session;

if (!Context.Contains("session")) //Validar que la sesion exista para no volverla a llamar
{
  //se inicia sesion de LLAPI
  //var session:LLSocketSession = new LLSocketSession(session2.Server, session2.Port, session2.Username, session2.Password, "");
  var session:LLSocketSession = new LLSocketSession(session2.Server, 2099, session2.Username, session2.Password, "");
  Context["session"] = session;
  //MessageBox.Show (session.GetServerInfo()); 
  //MessageBox.Show ("Conexión Correcta CS"); 
} 
else
{
  var session = Context["session"];
}


var doccuments:LapiDocuments = new LapiDocuments(session);

//Variables para ejecutar reporte
var listatributos:LLList = new LLList();
var atributo2:LLAssoc = new LLAssoc();

var resultado:LLRecArray = new LLRecArray();

//**********************************************************************************************************************************************************************************
var volid:long = -15733; //ID del Volumen
var nodeid:long  = 2416871;//ID del Reporte;
//**********************************************************************************************************************************************************************************

var poliza:String = Fields["330670.3:No. de póliza"].Value;

atributo2.Add("InputType", new LLString("String"));
atributo2.Add("Label", new LLString("inputLabel1"));
atributo2.Add("Value", new LLString(poliza));
atributo2.Add("Prompt", new LLString("No. Poliza"));

//Agregamos el atributo a la lista
listatributos.Add(atributo2);

try 
{
  resultado = doccuments.RunReport(volid, nodeid, listatributos);

  if (!resultado )//si no existe el archivador muestra error y sale
  {
    //MessageBox.Show ("No se encuentra el archivador correspondiente al No. de póliza "+poliza+", por favor verifique que se encuentre creado");
    //Fields["Archivador"].Value = "";
  }
  else if (resultado)  //Si existe el archivador
  {
    //Llenar Variables con los valores  
    var Ruta:String = resultado.GetValue(0,0).ToString().Replace("'","");
    var TipoDoc:String = resultado.GetValue(0,1).ToString().Replace("'","");
    var Doc:String = resultado.GetValue(0,2).ToString().Replace("'","");
    Fields["Archivador"].Value=Ruta;
    Fields[":SF_Reclamaciones_:330670.3:Tipo documento del asegurado"].Value=TipoDoc;
    Fields["330670.3:No. identificación del asegurado"].Value=Doc;
 }

}
catch(e)
{
  MessageBox.Show(e);
}
